package db

import "testing"

func init() {
	Init("/tmp")
	conn = Conn()
}

func TestAddCards(t *testing.T) {
	boardID, err := conn.AddBoard(BoardInfo{Name:"board1"})
	if err != nil {
		t.Fatalf("adding board 'board1' %s", err)
	}

	colID, err := conn.AddColumn(Column{Name:"new column"}, boardID)
	if err != nil {
		t.Fatalf("adding column %s to board %s %s", "new column", "board1", err)
	}

	card := Card{
		ColumnID: colID,
		Title:    "Test card",
		Body:     "Test card body",
		Creator:  "God",
	}

	cardID, err := conn.AddCard(card, boardID)
	if err != nil {
		t.Fatalf("adding card %v to board %d", card, boardID)
	}

	board, err := conn.GetBoard(boardID)
	if err != nil {
		t.Fatalf("getting board %d", boardID)
	}

	t.Logf("board %d: %+v", boardID, board)

	err = conn.DeleteCard(cardID, boardID)
	if err != nil {
		t.Fatalf("adding card %d in board %d", cardID, boardID)
	}

	err = conn.DeleteColumn(colID, boardID)
	if err != nil {
		t.Fatalf("adding column %d in board %d", colID, boardID)
	}

	err = conn.DeleteBoard(boardID)
	if err != nil {
		t.Fatalf("deleting board %d", boardID)
	}
}
