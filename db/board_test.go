package db

import "testing"

var conn *DB

func init() {
	Init("/tmp")
	conn = Conn()
}

func TestBoardCreation(t *testing.T) {
	boardID, err := conn.AddBoard(BoardInfo{Name:"new board"})
	if err != nil {
		t.Fatalf("creating 'new board' %s", err)
	}

	err = conn.DeleteBoard(boardID)
	if err != nil {
		t.Fatalf("deleting board %d %s", boardID, err)
	}
}
