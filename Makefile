kb:
	go build ./cmd/kb

debug:
	dlv exec  --headless --api-version 2 --listen=0.0.0.0:2345 ./kb

kb-cli:
	go build ./cmd/kb-cli

.PHONY: kb kb-cli debug
