package biz

import (
	"encoding/json"
	"net/http"

	"codeberg.org/wlcsm/kb/db"
)

type CreateCardReq struct {
	Card    db.Card `json:"card"`
	BoardID uint64 `json:"boardID"`
}

type CreateCardRes struct {
	ID uint64 `json:"id"`
}

func CreateCard(r *http.Request) (any, int, error) {
	var req CreateCardReq
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	conn := db.Conn()
	id, err := conn.AddCard(req.Card, req.BoardID)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return CreateCardRes{ID: id}, http.StatusOK, nil
}

type EditCardReq struct {
	Card  db.Card `json:"card"`
	BoardID uint64 `json:"boardID"`
}

type EditCardRes struct {
}

func EditCard(r *http.Request) (any, int, error) {
	var req EditCardReq
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	conn := db.Conn()

	err := conn.EditCard(req.Card, req.BoardID)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return EditCardRes{}, http.StatusOK, nil
}
