package biz

import (
	"net/http"
)

type UserDataRes struct {
	BoardID uint64 `json:"boardID"`
}

func GetUserData(r *http.Request) (any, int, error) {
	return UserDataRes{BoardID: 1}, http.StatusOK, nil
}
