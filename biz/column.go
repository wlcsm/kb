package biz

import "codeberg.org/wlcsm/kb/db"

type Column struct {
	ID    uint64    `json:"id"`
	Name  string    `json:"name"`
	Cards []db.Card `json:"cards"`
}

func UpdateColumn(boardID uint64, column Column) error {
	conn := db.Conn()

	dbCol := db.Column{
		ID: column.ID,
		Name: column.Name,
		CardIDs: make([]uint64, len(column.Cards)),
	}

	for i := range column.Cards {
		dbCol.CardIDs[i] = column.Cards[i].ID
	}

	return conn.EditColumn(dbCol, boardID)
}
