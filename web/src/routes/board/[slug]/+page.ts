import { GetBoard } from '$lib/api/board';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch, params }) => {
	try {
		let boardID = Number(params.slug);
		let resp = await GetBoard(fetch, boardID);
		return resp;
	} catch (e) {
		// TODO more advanced error handling to differentiate
		// internal server errors and not found errors
		throw error(404, 'Not found');
	}
};
