import { BaseURL, type FetchType } from './base';

const CardURL = BaseURL + '/card';

export type CardT = {
	id: number;
	columnID: number;
	title: string;
	body: string;
};

export type AddCardReq = {
	boardID: number;
	columnID: number;
	card: CardT;
};

export function AddCard(fetch: FetchType, req: AddCardReq): Promise<Response> {
	return fetch(CardURL, {
		method: 'POST',
		body: JSON.stringify(req)
	});
}

export type EditCardReq = {
	boardID: number;
	card: CardT;
};

export function EditCard(fetch: FetchType, req: EditCardReq): Promise<Response> {
	return fetch(CardURL, {
		method: 'PUT',
		body: JSON.stringify(req)
	});
}
