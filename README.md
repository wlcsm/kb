# kb - simple Kanban board

kb is a lighteight kanban board

## Persistent storage

`kb` uses the [bolt](https://github.com/boltdb/bolt) database system. The rationale being:

* Embedded dependency: included directory in the binary, no extra installation steps
* `kb` doesn't require complex queries. Key/values storage is all that is required

Stores data in $KB_DIR, otherwise $XDG_DATA_HOME. If both are empty, it panics.

We then store the data in a single file. With the following buckets:

* boards-{name}: bucket for a particular board

each board bucket contains two buckets

* columns: the columns
* items: autoincrement id


# Structure

Cards and Columns are held at the same level, rather than Columns containing cards. This is not due to an implementation detail, it is because card's ID is unique to the board, not it the column. That is, the Card's immediate surrounding structure is the Board, not the Column.