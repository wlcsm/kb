package db

import (
	"encoding/json"

	"github.com/boltdb/bolt"
	"github.com/pkg/errors"
)

type Column struct {
	ID      uint64   `json:"id"`
	Name    string   `json:"name"`
	CardIDs []uint64 `json:"cards"`
}

func (c Column) JSON() []byte {
	d, _ := json.Marshal(c)
	return d
}

func (d *DB) AddColumn(col Column, boardID uint64) (uint64, error) {
	var id uint64
	err := d.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if bkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		columnBkt := bkt.Bucket(ColumnBucketKey)

		id, _ = columnBkt.NextSequence()
		col.ID = id

		return columnBkt.Put(IDToKey(col.ID), col.JSON())
	})

	if err != nil {
		return 0, err
	}
	return id, nil
}

func (d *DB) EditColumn(column Column, boardID uint64) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if bkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		columnBkt := bkt.Bucket(ColumnBucketKey)

		return columnBkt.Put(IDToKey(column.ID), column.JSON())
	})
}

func (d *DB) DeleteColumn(columnID, boardID uint64) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if bkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		columnBkt := bkt.Bucket(ColumnBucketKey)

		return columnBkt.Delete(IDToKey(columnID))
	})
}
