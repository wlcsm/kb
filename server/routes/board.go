package biz

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/pkg/errors"

	"codeberg.org/wlcsm/kb/biz"
	"codeberg.org/wlcsm/kb/db"
)

func GetBoard(r *http.Request) (any, int, error) {
	idStr := r.URL.Query().Get("id")
	if len(idStr) == 0 {
		return nil, http.StatusBadRequest, errors.New("id query param can't be empty")
	}
	id, err := strconv.ParseUint(idStr, 10, 64)
	if err != nil {
		return nil, http.StatusBadRequest, errors.Wrap(err, "parsing id")
	}

	board, err := biz.GetBoard(id)
	if err != nil {
		return board, http.StatusInternalServerError, err
	}

	return board, http.StatusOK, err
}

func GetBoardList(r *http.Request) (any, int, error) {
	conn := db.Conn()

	boards, err := conn.GetBoardList()
	if err != nil {
		return boards, http.StatusInternalServerError, err
	}

	return boards, http.StatusOK, err
}

type CreateBoardReq struct {
	Name string `json:"name"`
}

type CreateBoardRes struct {
	ID uint64 `json:"id"`
}

func CreateBoard(r *http.Request) (any, int, error) {
	var req CreateBoardReq
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	boardInfo := db.BoardInfo{
		Name: req.Name,
	}

	id, err := db.Conn().AddBoard(boardInfo)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return CreateBoardRes{ID: id}, http.StatusOK, nil
}
