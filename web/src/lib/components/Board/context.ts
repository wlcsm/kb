import type { BoardT } from '$lib/api/board';
import { setContext, getContext } from 'svelte';

type BoardContext = {
	board: BoardT;
};

const contextKey = Symbol();

export const getBoardContext = () => getContext<BoardContext>(contextKey);

export const setBoardContext = (b: BoardContext) => setContext(contextKey, b);
