package db

import "testing"

func init() {
	Init("/tmp")
	conn = Conn()
}

func TestAddColumns(t *testing.T) {
	boardID, err := conn.AddBoard(BoardInfo{Name:"board1"})
	if err != nil {
		t.Fatalf("adding board 'board1' %s", err)
	}

	colID, err := conn.AddColumn(Column{Name:"new column"}, boardID)
	if err != nil {
		t.Fatalf("adding column %s to board %s %s", "new column", "board1", err)
	}

	err = conn.DeleteColumn(colID, boardID)
	if err != nil {
		t.Fatalf("deleting column %s to board %s %s", "new column", "board1", err)
	}
}
