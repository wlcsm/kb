package db

import (
	"encoding/json"

	"github.com/boltdb/bolt"
	"github.com/pkg/errors"
)

type Card struct {
	// board unique identifier. Unqiue for a given board
	ID       uint64 `json:"id"`
	ColumnID uint64 `json:"columnID"`
	Title    string `json:"title"`
	Body     string `json:"body"`
	Creator  string `json:"creator"`
}

func (c Card) JSON() []byte {
	d, _ := json.Marshal(c)
	return d
}

func (d *DB) AddCard(card Card, boardID uint64) (uint64, error) {
	var id uint64

	err := d.db.Update(func(tx *bolt.Tx) error {
		boardBkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if boardBkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		cardBkt := boardBkt.Bucket(CardBucketKey)
		card.ID, _ = cardBkt.NextSequence()

		err := cardBkt.Put(IDToKey(card.ID), card.JSON())
		if err != nil {
			return errors.Wrapf(err, "saving card %s in board %d", string(card.JSON()), boardID)
		}

		return nil
	})

	if err != nil {
		return 0, err
	}
	return id, nil
}

func (d *DB) EditCard(card Card, boardID uint64) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		boardBkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if boardBkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		cardBkt := boardBkt.Bucket(CardBucketKey)

		return cardBkt.Put(IDToKey(card.ID), card.JSON())
	})
}

func (d *DB) DeleteCard(cardID, boardID uint64) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		boardBkt := tx.Bucket(BoardBucketKey).Bucket(IDToKey(boardID))
		if boardBkt == nil {
			return errors.Errorf("board doesn't exist: %d", boardID)
		}

		cardBkt := boardBkt.Bucket(CardBucketKey)

		err := cardBkt.Delete(IDToKey(cardID))
		if err != nil {
			return errors.Wrapf(err, "deleting card %d in board %d", cardID, boardID)
		}

		return nil
	})
}
