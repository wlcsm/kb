package db

import (
	"path/filepath"
	"strconv"

	"github.com/boltdb/bolt"
	"sync/atomic"
)

const (
	DBFilename = "kb.db"
)

var (
	BoardBucketKey = []byte("boards")

	ColumnBucketKey = []byte("columns")
	CardBucketKey   = []byte("cards")
	BoardInfokey    = []byte("info")
)

type DB struct {
	db *bolt.DB
}

var dbConn = &DB{}

var initialised atomic.Bool

func AlreadyInitialised() bool {
	return initialised.Swap(true)
}

func Init(baseDir string) error {
	if AlreadyInitialised() {
		return nil
	}

	dbPath := filepath.Join(baseDir, DBFilename)

	var db *bolt.DB
	db, err := bolt.Open(dbPath, 0600, nil)
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey)
		if bkt != nil {
			return nil
		}

		_, _ = tx.CreateBucket(BoardBucketKey)

		return err
	})

	dbConn = &DB{db}

	return err
}

func Conn() *DB {
	return dbConn
}

func IDToKey(id uint64) []byte {
	return []byte(strconv.Itoa(int(id)))
}
