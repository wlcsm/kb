import { BaseURL, type FetchType } from './base';
import type { CardT } from './card';

const ColumnPath = '/column';

export type ColumnT = {
	id: number;
	name: string;
	cards: CardT[];
};

type AddColumnReq = {
	boardID: number;
	index: number;
	column: ColumnT;
};

export function AddColumn(fetch: FetchType, req: AddColumnReq): Promise<Response> {
	let url = BaseURL + ColumnPath;

	return fetch(url, {
		method: 'POST',
		body: JSON.stringify(req)
	});
}

type EditColumnReq = {
	boardID: number;
	column: ColumnT;
};

export function UpdateColumn(fetch: FetchType, req: EditColumnReq): Promise<Response> {
	let url = BaseURL + ColumnPath;

	return fetch(url, {
		method: 'PUT',
		body: JSON.stringify(req)
	});
}

type DeleteComlumnReq = {
	boardID: number;
	columnID: number;
};

export function DeleteColumn(fetch: FetchType, req: DeleteComlumnReq): Promise<Response> {
	let url = BaseURL + ColumnPath + '?board_id=' + req.boardID + '&column_id=' + req.columnID;

	return fetch(url, {
		method: 'DELETE'
	});
}
