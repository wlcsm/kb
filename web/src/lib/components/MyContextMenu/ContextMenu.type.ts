export type Option = {
	text: string;
	callback: () => void;
};

export type ContextMenuParams = {
	x: number;
	y: number;
	opts: Option[];
};

export type ContextMenuContext = {
	open: (c: ContextMenuParams) => void;
};
