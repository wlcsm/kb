export const BaseURL = `/api`;

// type signature of the fetch function.
// Used since our functions accept a generic fetch function.
// This allows us to either use the default fetch function, or
// SvelteKit's fetch function as provided during 'load'
export type FetchType = (
	input: RequestInfo | URL,
	init?: RequestInit | undefined
) => Promise<Response>;
