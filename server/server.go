package server

import (
	"log"
	"net/http"
	"os"

	"codeberg.org/wlcsm/kb/server/routes"
)

var (
	Port = "8080"

	FrontendFiles = "./web/dist"
)

func InitConstants() {
	if port := os.Getenv("PORT"); len(port) != 0 {
		Port = port
	}
}

func Start() {
	InitConstants()

	http.Handle("/api/board", &MethodMux{
		GET:  biz.GetBoard,
		POST: biz.CreateBoard,
	})
	http.Handle("/api/board/list", &MethodMux{
		GET: biz.GetBoardList,
	})

	http.Handle("/api/column", &MethodMux{
		POST:   biz.CreateColumn,
		PUT:    biz.EditColumn,
		DELETE: biz.DeleteColumn,
	})

	http.Handle("/api/card", &MethodMux{
		POST: biz.CreateCard,
		PUT:  biz.EditCard,
	})

	http.Handle("/api/user_data", &MethodMux{
		GET: biz.GetUserData,
	})

	http.Handle("/", wrapper{http.StripPrefix("/", http.FileServer(http.Dir(FrontendFiles)))})

	log.Println("hosting HTTP service on port " + Port)
	log.Fatal(http.ListenAndServe(":"+Port, nil))
}
