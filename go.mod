module codeberg.org/wlcsm/kb

go 1.19

require (
	github.com/boltdb/bolt v1.3.1
	github.com/pkg/errors v0.9.1
)

require (
	github.com/felixge/fgprof v0.9.3 // indirect
	github.com/google/pprof v0.0.0-20211214055906-6f57359322fd // indirect
	golang.org/x/exp v0.0.0-20221114191408-850992195362
)

require (
	github.com/pkg/profile v1.7.0
	golang.org/x/sys v0.1.0 // indirect
)
