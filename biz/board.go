package biz

import "codeberg.org/wlcsm/kb/db"

// A hierachical representation of the board more akin to what will be
// displayed to the user.
type Board struct {
	ID      uint64   `json:"id"`
	Name    string   `json:"name"`
	Columns []Column `json:"columns"`
}

func GetBoard(id uint64) (*Board, error) {
	conn := db.Conn()

	dbBoard, err := conn.GetBoard(id)
	if err != nil {
		return nil, err
	}

	board := fromDBBoard(dbBoard)

	return board, nil
}

// Converts the database's board model into a hierachical structure
func fromDBBoard(b db.Board) *Board {
	board := Board{
		ID:   b.ID,
		Name: b.Name,
	}

	cols := make([]Column, len(b.Columns))

	// Maps the column ID to its index inside the 'cols' slice
	colIdToIndex := make(map[uint64]int, len(b.Columns))

	for i, column := range b.Columns {
		cols[i] = Column{
			ID:    column.ID,
			Name:  column.Name,
			Cards: make([]db.Card, 0, len(column.CardIDs)),
		}

		colIdToIndex[column.ID] = i
	}

	for _, card := range b.Cards {
		index := colIdToIndex[card.ColumnID]

		cols[index].Cards = append(cols[index].Cards, card)
	}

	board.Columns = cols

	return &board
}
