package biz

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"codeberg.org/wlcsm/kb/biz"
	"codeberg.org/wlcsm/kb/db"
)

type CreateColumnReq struct {
	Column  db.Column `json:"column"`
	BoardID uint64    `json:"boardID"`
}

type CreateColumnRes struct {
	ID uint64 `json:"id"`
}

func CreateColumn(r *http.Request) (any, int, error) {
	var req CreateColumnReq
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	conn := db.Conn()

	id, err := conn.AddColumn(req.Column, req.BoardID)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return CreateColumnRes{ID: id}, http.StatusOK, nil
}

type EditColumnReq struct {
	Column  biz.Column `json:"column"`
	BoardID uint64     `json:"boardID"`
}

type EditColumnRes struct {
}

func EditColumn(r *http.Request) (any, int, error) {
	var req EditColumnReq
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, http.StatusBadRequest, err
	}

	err := biz.UpdateColumn(req.BoardID, req.Column)
	if err != nil {
		return EditColumnRes{}, http.StatusInternalServerError, err
	}

	return EditColumnRes{}, http.StatusOK, nil
}

type DeleteColumnReq struct {
	ColumnID uint64 `query:"column_id"`
	BoardID  uint64 `query:"board_id"`
}

type DeleteColumnRes struct {
}

func DeleteColumn(r *http.Request) (any, int, error) {
	var req DeleteColumnReq
	var err error
	req.ColumnID, err = GetUint64(r, "column_id")
	if err != nil {
		return nil, http.StatusBadRequest, err
	}
	req.BoardID, err = GetUint64(r, "board_id")
	if err != nil {
		return nil, http.StatusBadRequest, err
	}

	conn := db.Conn()

	err = conn.DeleteColumn(req.ColumnID, req.BoardID)
	if err != nil {
		return nil, http.StatusInternalServerError, err
	}

	return DeleteColumnRes{}, http.StatusOK, nil
}

func GetUint64(r *http.Request, key string) (uint64, error) {
	res := r.URL.Query().Get(key)
	if len(res) == 0 {
		return 0, fmt.Errorf("query parameter " + key + " not given")
	}

	v, err := strconv.ParseUint(res, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("parsing query parameter "+key+"="+res+"as unsigned integer: %w", err)
	}

	return v, nil
}
