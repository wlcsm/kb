package db

import (
	"encoding/json"

	"github.com/boltdb/bolt"
	"github.com/pkg/errors"
)

type Board struct {
	BoardInfo
	Columns []Column `json:"columns"`
	Cards   []Card   `json:"cards"`
}

type BoardInfo struct {
	ID   uint64 `json:"id"`
	Name string `json:"name"`
}

var (
	ErrBoardNotExist = errors.New("board doesn't exist")
)

func (d *DB) GetBoard(id uint64) (Board, error) {
	var board Board

	err := d.db.View(func(tx *bolt.Tx) (err error) {
		bkt := tx.Bucket(BoardBucketKey)

		boardBkt := bkt.Bucket(IDToKey(id))
		if boardBkt == nil {
			return errors.Wrapf(ErrBoardNotExist, "boardID=%d", id)
		}

		board.Columns, err = getAllColumns(boardBkt)
		if err != nil {
			return err
		}

		board.Cards, err = getAllCards(boardBkt)
		if err != nil {
			return err
		}

		board.BoardInfo, err = getBoardMeta(boardBkt)
		return err
	})
	if err != nil {
		return Board{}, err
	}

	return board, nil
}

func getBoardMeta(boardBkt *bolt.Bucket) (BoardInfo, error) {
	metaBytes := boardBkt.Get(BoardInfokey)
	var boardMeta BoardInfo

	err := json.Unmarshal(metaBytes, &boardMeta)
	return boardMeta, err
}

func getAllColumns(bkt *bolt.Bucket) ([]Column, error) {
	columnBkt := bkt.Bucket(ColumnBucketKey)
	cols := make([]Column, columnBkt.Stats().KeyN)

	i := 0
	err := columnBkt.ForEach(func(k, v []byte) error {
		if err := json.Unmarshal(v, &cols[i]); err != nil {
			return errors.Wrapf(err, "marshalling column %s", string(k))
		}
		i++

		return nil
	})
	if err != nil {
		return nil, err
	}

	return cols, nil
}

func getAllCards(boardBkt *bolt.Bucket) ([]Card, error) {
	cardBkt := boardBkt.Bucket(CardBucketKey)
	cards := make([]Card, cardBkt.Stats().KeyN)

	i := 0
	err := cardBkt.ForEach(func(k, v []byte) error {
		if err := json.Unmarshal(v, &cards[i]); err != nil {
			return errors.Wrapf(err, "marshalling card %s in board %d", string(k))
		}
		i++

		return nil
	})
	if err != nil {
		return nil, err
	}

	return cards, nil
}

func (d *DB) GetBoardList() ([]BoardInfo, error) {
	var boards []BoardInfo

	err := d.db.View(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey)

		// BucketN includes the top bucket itself, so we subtract 1
		numBuckets := bkt.Stats().BucketN - 1
		boards = make([]BoardInfo, 0, numBuckets)

		var boardMeta BoardInfo

		return bkt.ForEach(func(k, v []byte) error {
			// k may be pointing to a key rather than a bucket
			b := bkt.Bucket(k)
			if b == nil {
				return nil
			}

			meta := b.Get(BoardInfokey)
			_ = json.Unmarshal(meta, &boardMeta)

			boards = append(boards, boardMeta)
			return nil
		})
	})

	return boards, err

}

func (d *DB) AddBoard(info BoardInfo) (id uint64, err error) {
	err = d.db.Update(func(tx *bolt.Tx) error {
		bkt := tx.Bucket(BoardBucketKey)

		// The CreateBucket method will only return an error if the key
		// already exists, is blank, or is too long. hence it will not
		// error here
		id, _ := bkt.NextSequence()
		boardBkt, _ := bkt.CreateBucket(IDToKey(id))

		_, _ = boardBkt.CreateBucket(CardBucketKey)
		_, _ = boardBkt.CreateBucket(ColumnBucketKey)

		boardInfo := BoardInfo{
			ID:   id,
			Name: info.Name,
		}

		data, _ := json.Marshal(boardInfo)

		_ = boardBkt.Put(BoardInfokey, data)
		return err
	})
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (d *DB) DeleteBoard(id uint64) error {
	return d.db.Update(func(tx *bolt.Tx) error {
		return tx.Bucket(BoardBucketKey).DeleteBucket(IDToKey(id))
	})
}
