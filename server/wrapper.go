package server

import (
	"encoding/json"
	"log"
	"net/http"
	"sync/atomic"

	"golang.org/x/exp/slog"
)

// Wraps a handler to add the necessary middleware
type wrapper struct {
	d http.Handler
}

func (h wrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger := slog.FromContext(r.Context()).With(
		"method", r.Method,
		"url", r.URL,
		"traceID", getTraceID(),
	)
	r = r.WithContext(slog.NewContext(r.Context(), logger))

	logger.Info("received request")

	h.d.ServeHTTP(w, r)
}

// Very tiny Mux for HTTP methods
type MethodMux struct {
	GET, POST, PUT, DELETE handlerFunc
}

var counter atomic.Uint64

func getTraceID() uint64 {
	return counter.Add(1)
}

type Response struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
	Data  any    `json:"data"`
}

func (r Response) LogValue() slog.Value {
    return slog.GroupValue(
        slog.Int("code", r.Code),
        slog.Any("data", r.Data),
        slog.String("error", r.Error),
    )
}

type handlerFunc func(r *http.Request) (any, int, error)

func (m *MethodMux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger := slog.FromContext(r.Context()).With(
		"method", r.Method,
		"url", r.URL,
		"traceID", getTraceID(),
	)
	r = r.WithContext(slog.NewContext(r.Context(), logger))

	logger.Info("received request")

	// need to set the text/plain for now since we are doing CORS in debugging
	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var handler handlerFunc

	switch {
	case r.Method == "GET" && m.GET != nil:
		handler = m.GET
	case r.Method == "POST" && m.POST != nil:
		handler = m.POST
	case r.Method == "PUT" && m.PUT != nil:
		handler = m.PUT
	case r.Method == "DELETE" && m.DELETE != nil:
		handler = m.DELETE
	case r.Method == "OPTIONS":
		// for CORS preflight responses
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
		return
	default:
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}

	var resp Response
	v, code, err := handler(r)
	if err != nil {
		resp.Error = err.Error()
	} else {
		resp.Data = v
	}

	w.WriteHeader(code)
	resp.Code = code

	logger.Info("send response", "resp", resp)

	if err := json.NewEncoder(w).Encode(resp); err != nil {
		log.Printf("ERR: writing response body: %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
