import { GetBoardList } from '$lib/api/board';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';

export const load: PageLoad = async ({ fetch }) => {
	try {
		let resp = await GetBoardList(fetch);
		return resp;
	} catch (e) {
		// TODO more advanced error handling to differentiate
		// internal server errors and not found errors
		throw error(500, 'Not found');
	}
};
