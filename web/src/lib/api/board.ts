import { BaseURL, type FetchType } from './base';
import type { ColumnT } from './column';

const BoardURL = BaseURL + '/board';

export type BoardT = {
	id: number;
	name: string;
	columns: ColumnT[];
};

export type BoardInfoT = {
	id: number;
	name: string;
};

type CreateBoardRes = {
	board: BoardT;
};

export function CreateBoard(fetch: FetchType, name: string): Promise<CreateBoardRes> {
	return fetch(BoardURL, {
		method: 'POST',
		body: JSON.stringify({
			name: name
		})
	}).then((r) => r.json());
}

type GetBoardRes = BoardT;

export function GetBoard(fetch: FetchType, boardID: number): Promise<GetBoardRes> {
	return fetch(BoardURL + '?id=' + boardID, {
		method: 'GET'
	})
		.then((r) => r.json())
		.then((r) => r.data);
}

type BoardListRes = BoardInfoT[];

export function GetBoardList(fetch: FetchType): Promise<BoardListRes> {
	return fetch(BoardURL + '/list', {
		method: 'GET'
	})
		.then((r) => r.json())
		.then((r) => r.data)
		.catch((e) => console.error(e));
}
