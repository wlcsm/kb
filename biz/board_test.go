package biz

import (
	"reflect"
	"testing"

	"codeberg.org/wlcsm/kb/db"
)

func TestNewBoard(t *testing.T) {
	tests := []struct {
		name    string
		dbBoard db.Board
		want    *Board
	}{
		{
			name: "Simple",
			dbBoard: db.Board{
				BoardInfo: db.BoardInfo{
					ID:   1,
					Name: "TestBoard",
				},
				Columns: []db.Column{
					{
						ID:      1,
						Name:    "TODO",
						CardIDs: []uint64{1, 2},
					},
				},
				Cards: []db.Card{
					{
						ID:       1,
						ColumnID: 1,
						Title:    "MyTestCard",
						Body:     "body",
						Creator:  "me",
					},
					{
						ID:       2,
						ColumnID: 1,
						Title:    "MyTestCard2",
						Body:     "body2",
						Creator:  "me2",
					},
				},
			},
			want: &Board{
				ID:   1,
				Name: "TestBoard",
				Columns: []Column{{
					ID:   1,
					Name: "TODO",
					Cards: []db.Card{
						{
							ID:       1,
							ColumnID: 1,
							Title:    "MyTestCard",
							Body:     "body",
							Creator:  "me",
						},
						{
							ID:       2,
							ColumnID: 1,
							Title:    "MyTestCard2",
							Body:     "body2",
							Creator:  "me2",
						},
					},
				}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fromDBBoard(tt.dbBoard); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewStructuredBoard() = %+v\n, want %+v", got, tt.want)
			}
		})
	}
}
